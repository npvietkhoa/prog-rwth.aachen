% Aufgabe 7
multiples(X, _, []) :- X < 1.
multiples(_, Y, []) :- Y < 1.
multiples(X, Y, [Z | ZS]) :-
    0 < X,
    0 < Y,
    Z is X * Y,
    Y1 is Y - 1,
    multiples(X, Y1, ZS).


