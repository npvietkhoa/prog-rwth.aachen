% Aufgabe 9

blau(sonnalm).
blau(arbiskogel).
blau(plattenalm).
blau(wiesenalm).
blau(vorkogel).

rot(isskogel).

schwarz(teufeltal).

start(sonnalm).
start(teufeltal).

endetIn(sonnalm, vorkogel).
endetIn(sonnalm, arbiskogel).
endetIn(teufeltal, wiesenalm).
endetIn(arbiskogel, plattenalm).
endetIn(plattenalm, wiesenalm).
endetIn(vorkogel, isskogel).
endetIn(isskogel, tal).
endetIn(wiesenalm, tal).


% Teilaufgabe a
pathOfLength(cons(tal, nil), 0).
pathOfLength(cons(X, cons(Y, Rest)), s(L)) :- endetIn(X, Y), pathOfLength(cons(Y, Rest), L).
 
% Teilaufgabe b
append(nil, XS, XS).
append(cons(X, Rest), YS, cons(X, XS)) :- append(Rest, YS, XS).

add(X, 0, X).
add(X, s(Y), s(Z)) :- add(X,Y,Z).

tourOfLength(cons(tal, nil), 0).
tourOfLength(cons(tal, Rest), L) :- 
    pathOfLength(cons(X, XS), L_X), start(X), add(L_X, L_Y, L), tourOfLength(cons(tal, YS), L_Y), append(cons(X,XS), YS, Rest).

%Teilaufgabe d
convert(nil, []).
convert(cons(X, nil), [X]).
convert(cons(X, Rest), [X | ZS]) :- convert(Rest, ZS).

