-- Aufgabe 3

dropMult :: Int -> [Int] -> [Int]
dropMult x xs = filter (\y -> mod y x /= 0 ) xs

dropAll :: [Int] -> [Int]
dropAll (x : xs) = x : dropAll (dropMult x xs)

primes :: [Int]
primes = dropAll [2..]

productPrimes :: [Int]
productPrimes = zipWith (*) (drop 2 primes) primes

getBigger :: [Int] -> [Int] -> [Int]
getBigger [] _ = []
getBigger _ [] = []
getBigger (x : xs) (y : ys) | (x * x) > y = x : getBigger xs ys
                            | otherwise = getBigger xs ys

perfectPrimes :: [Int]
perfectPrimes = getBigger (drop 1 primes) productPrimes
