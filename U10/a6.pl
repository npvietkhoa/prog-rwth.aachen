% Aufgabe 6
% Teilaufgabe a

% Teilaufgabe a - blau(X). rot(X). schwarz(X)
blau(sonnalm).
blau(arbiskogel).
blau(plattenalm).
blau(wiesenalm).
blau(vorkogel).

rot(isskogel).

schwarz(teufeltal).

% Teilaufgabe a - start(X)
start(sonnalm).
start(teufeltal).

% Teilaufgabe a - endetIn(X,Y)
endetIn(sonnalm, vorkogel).
endetIn(sonnalm, arbiskogel).
endetIn(teufeltal, wiesenalm).
endetIn(arbiskogel, plattenalm).
endetIn(plattenalm, wiesenalm).
endetIn(vorkogel, isskogel).
endetIn(isskogel, tal).
endetIn(wiesenalm, tal).

% Teilaufgabe b
% endetIn(X, wiesenalm).

% Teilaufgabe c
gleicherStartpunkt(X, Y) :- start(X), start(Y).
gleicherStartpunkt(X, Y) :- endetIn(StartPunkt, X), endetIn(StartPunkt, Y).

% Teilaufgabe d
erreichbar(X, Y) :- endetIn(X, Y).
erreichbar(X, Y) :- endetIn(X, Z), erreichbar(Z, Y), erreichbar(Z, tal).

% Teilaufgabe e
moeglicheSchlusspist(X, S) :- endetIn(X, S), endetIn(S, tal).
moeglicheSchlusspist(X, S) :- endetIn(X, Z), moeglicheSchlusspist(Z, S), endetIn(S, tal).

% Teilaufgabe f
treffpisten(X, Y, T) :- erreichbar(X, T), erreichbar(Y, T), T \= tal.

% Teilaufgabe g
anfaengerGeeignet(X) :- blau(X), endetIn(X, tal).
anfaengerGeeignet(X) :- endetIn(S ,tal), blau(S), moeglicheSchlusspist(X, S), blau(X).
