first :: [Int] -> Int
first [] = 0
first (x : xs) = x

tillN :: Int -> [Int]
tillN n = if (n <= 0) then []
	else n : tillN (n - 1)

greaterThan :: [Int] -> Int -> [Int]
greaterThan [] _ = []
greaterThan (x : xs) n = if (x > n) then x : (greaterThan xs n)
else (greaterThan xs n)
