-- Aufgabe 8

-- Teilaufgabe a

isMatrix :: [[Int]] -> Bool
isMatrix [] = False 
isMatrix (row : []) = length row > 0
isMatrix (firstRow : rest) = length firstRow == length (rest !! 0) && isMatrix rest

-- Teilaufgabe b
dimensions :: [[Int]] -> (Int, Int)
dimensions matrix | isMatrix matrix = (length matrix, length (matrix !! 0))
                  | otherwise = (-1, -1)

-- Teilaufgabe c
isQuadratic :: [[Int]] -> Bool 
isQuadratic matrix  | hasRowAsCol (dimensions matrix) = True
                    | otherwise  = False 
    where hasRowAsCol :: (Int, Int) -> Bool 
          hasRowAsCol (-1, -1) = False 
          hasRowAsCol (firstDim, secondDim) = firstDim == secondDim

-- Teilaufgabe d
getRow :: [[Int]] -> Int -> [Int]
getRow matrix i | isMatrix matrix && (i <= length matrix) && (i > 0) =  matrix !! (i - 1)
                | otherwise = errorWithoutStackTrace "Given information is valid"

getCol :: [[Int]] -> Int -> [Int]
getCol matrix i | isMatrix matrix && (i <= length (matrix !! 0)) && (i > 0)
                  = getEleEachRowInPos matrix (i - 1)
                | otherwise = errorWithoutStackTrace "Given information is invalid"
    where getEleEachRowInPos :: [[Int]] -> Int -> [Int]
          getEleEachRowInPos (lastRow : []) pos = (lastRow !! pos) : []
          getEleEachRowInPos (row : rest) pos = (row !! pos) : getEleEachRowInPos rest pos

-- Teilaufgabe e
trav :: [[Int]] -> [[Int]]
trav matrix | isMatrix matrix = getColInPos matrix (matrix !! 0)
            | otherwise = errorWithoutStackTrace "Given rowlist is not a matrix"
    where getColInPos :: [[Int]] -> [Int] -> [[Int]]
          getColInPos givenMatrix [] = []
          getColInPos givenMatrix (ele : rest) = getCol givenMatrix (rowLength - length rest) : getColInPos givenMatrix rest 
          
            where rowLength = length (givenMatrix !! 0)



-- Teilaufgabe f
setEntry :: [[Int]] -> Int -> Int -> Int -> [[Int]]
setEntry matrix i j aij | isMatrix matrix && (i > 0 && i <= maxRow) && (j > 0 && j <= maxCol)
                          = setIntoList matrix (setIntoList (matrix !! (i - 1)) aij j) i
                        | otherwise  = errorWithoutStackTrace "Given information is invalid"
    where maxCol = length (matrix !! 0)
          maxRow = length matrix 
          
          setIntoList :: [a] -> a -> Int -> [a]
          setIntoList (ele : rest) eleToPut 1 = eleToPut : rest
          setIntoList (ele : rest) eleToPut pos = ele : setIntoList rest eleToPut (pos - 1) 



-- Teilaufgabe g
{- addMatrices funktion nimmt 2 Matrizen als Parameter und berechnen die Summematrix, 
    falls die gegebene Matrizen gleiche Dimensions haben. Am sonst werf Exception
-}

addMatrices :: [[Int]] -> [[Int]] -> [[Int]]
addMatrices givenMatrix1 givenMatrix2 | haveSameDimensions givenMatrix1 givenMatrix2 = add givenMatrix1 givenMatrix2
                                      | otherwise = errorWithoutStackTrace "Two matrices do not have same dimensions"
    where len = length (givenMatrix2 !! 0)
          
          add :: [[Int]] -> [[Int]] -> [[Int]]
          add [] givenMatrix2 = []
          add (row : restMatrix1) givenMatrix2 = addRows row (givenMatrix2 !! (len - length restMatrix1 - 1)) : add restMatrix1 givenMatrix2
            where len = length givenMatrix2

          addRows :: [Int] -> [Int] -> [Int]
          addRows [] row2 = []
          addRows (ele : restRow1) row2 = (ele + row2 !! ((len - length restRow1) - 1)) : addRows restRow1 row2
            where len = length row2

          haveSameDimensions :: [[Int]] -> [[Int]] -> Bool
          haveSameDimensions matrix1 matrix2 = dimensions matrix1 == dimensions matrix2
    
