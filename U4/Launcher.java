/**
 * Objekte dieser Klasser repraesentieren main methode
 * @author Khoa Phuc Viet Nguyen
 */
public class Launcher {
    public static void main(String[] args) {
        Product apfel = new Product("Apfel", null, "111111111111111101");
        Product banana = new Product("Banana", "110001101110", "110011101001");
        Product ananas = new Product("Ananas", "000001110000", "000011111100");
        Product blumenkohl = new Product("Bluemenkohl", "110000000111", "000001110000");
        Product chinakohl = new Product("Chinakohl", "110001011010", "100010011101");
        Product canabis = new Product("Canabis" , "0", null); //testcase fuer Ausnahme

        SeasonalCalendar ssCalendar = new SeasonalCalendar(new Product[] {apfel, banana, blumenkohl, ananas, chinakohl, canabis});
        System.out.println(ssCalendar.stringify());
    }
}
