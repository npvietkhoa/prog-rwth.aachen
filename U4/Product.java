/**
 * Product
 * Objekte dieser Klasser repraesentieren eine Produkte - saisonale Ware
 */
public class Product {

    /**
     * Name des Produkts.
     */
    private String productName;

    /**
     * Array der Monate, wann das Produkt indiesen als Regionalware verfuegbar ist.
     */
    private boolean[] isRegionalAvailableInMonth = new boolean[12];

    /**
     *  Array der Monate, wann das Produkt indiesen als Importware verfuegbar ist.
     */
    private boolean[] isImportAvailableInMonth = new boolean[12];
    
    /**
     * Erstelle neue Produkt mit Array der vorgegbenen verfuegbaren Monaten von Regionalware und Importware.
     * @param productName Name des Produkts.
     * @param isRegionalAvailableInMonth Die Monate, wann das Produkt indiesen als Regionalware verfuegbar ist.
     * @param isImportAvailableInMonth Die Monate, wann das Produkt indiesen als Importware verfuegbar ist.
     */
    Product(String productName, boolean[] isRegionalAvailableInMonth, boolean[] isImportAvailableInMonth) {

        if (productName == null) {
            this.productName = "";
        } else {
            this.productName = productName;
        }

        // wenn variable sind uengultig, dann normaliesieren den Array.
        if (isImportAvailableInMonth == null ||
            isRegionalAvailableInMonth == null ||
            isImportAvailableInMonth.length != 12 || 
            isRegionalAvailableInMonth.length != 12) {
            
            this.isImportAvailableInMonth = normalizedBooleanArray(isImportAvailableInMonth);
            this.isRegionalAvailableInMonth = normalizedBooleanArray(isRegionalAvailableInMonth);
        } else {
            this.isRegionalAvailableInMonth = isRegionalAvailableInMonth.clone();
            this.isImportAvailableInMonth = isImportAvailableInMonth.clone();
        }
    }

    /**
     * Normaliziert gegebene boolesche Array, damit es genau 12 Elemente hat.
     * Falls Array ist null, fuelle alle Elemente mit false hinzu.
     * Falls Array hat mehr als 12 Elemente, nimmt nur 12 Elemente und den Rest weg.
     * Falls Array hat weniger als 12 Elemente, fuelle den Rest mit false bis wann es genug 12 Elemente hat.
     * @param booleanArr Gegebene Array.
     * @return normalizertes Array.
     */
    static boolean[] normalizedBooleanArray(boolean[] booleanArr) {
        boolean[] resultBooleanArr = new boolean[12];
        // if given Array is null, then fill all the result with element false
        if (booleanArr == null) {
            for (boolean b : resultBooleanArr) {
                b = false;
            }
            return resultBooleanArr;
        }

        // if the given Array is longer than 12 then trim the rest up
        if (booleanArr.length > 12) {
            for (int i = 0; i < 12; i++) {
                resultBooleanArr[i] = booleanArr[i];
            }
        } else {
            //if the given array has fewer than 12 Elements then copy it to result and fill the rest with false
            resultBooleanArr = booleanArr.clone();
            for (int i = booleanArr.length; i < resultBooleanArr.length; i++) {
                resultBooleanArr[i] = false;
            }
        }
        return resultBooleanArr;
    }


    /**
     * Erstelle neue Produkt mit String der vorgegbenen verfuegbaren Monaten von Regionalware und Importware.
     * @param productName Name des Produkts.
     * @param regionalAvailableInMonths String, der zeigt, wann Regionalware verfuegbar ist.
     * @param importAvailableInMonths String, der zeigt, wann Importware verfuegbar ist.
     */
    Product(String productName, String regionalAvailableInMonths, String importAvailableInMonths) {
        this(productName, 
            BooleanStringHelper.parse(regionalAvailableInMonths, '1'), 
            BooleanStringHelper.parse(importAvailableInMonths, '1'));
    }


    /**
     * Lese Name des Produkts
     * @return Name des Produkts
     */
    public String getName() {
        return this.productName;
    }

    /**
     * Umwandeln boolesche Array der Monate, 
     * wann das Produkt indiesen als Regionalware verfuegbar ist, zu lesbareres Form
     * @return String der Monate, wann das Produkt indiesen als Regionalware verfuegbar ist.
     */
    String stringifyIsRegional() {
        return BooleanStringHelper.stringify(this.isRegionalAvailableInMonth, 'R', ' ');
    }


    /**
     * Umwandeln boolesche Array der Monate, 
     * wann das Produkt indiesen als Importware verfuegbar ist, zu lesbareres Form
     * @return String der Monate, wann das Produkt indiesen als Importware verfuegbar ist.
     */
    String stringifyIsImported() {
        return BooleanStringHelper.stringify(this.isImportAvailableInMonth, 'I', ' ');
    }

}