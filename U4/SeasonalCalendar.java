/**
 * Objekte dieser Klasser repraesentieren die seasonal Kalendar
 */
public class SeasonalCalendar {

    /**
     * Array, das die Produkte repraesentiert
     */
    private Product[] products;


   
    /**
     * Setz neue seasonale Kalendar mit den Produkten 
     * @param products Produkte im Jahr
     */
    SeasonalCalendar(Product[] products) {
        this.products = products.clone();
    }


    /**
     * Finden die groesste Laenge der Name des Produkte in Array 
     * @return die groesste Laenge der Name des Produkte in Array
     */
    private int getLongestProductName() {
        int maxLength = 0;
        for (Product product : this.products) {
            maxLength = Math.max(maxLength, product.getName().length());
        }
        return maxLength;
    }

    /**
     * Fuegen in dem vorgegeben String die vorgegebene Buchstaben hinzu
     * bis wann die Laenge des String gleich die vorgegebene Laenge
     * @param inString Vorgegeber String
     * @param requiredLength Die Leange, die hinzugefuegter String erreichen muss
     * @param insertedChar Der Buchstabe, den hinzugefuegt nach vorgegeben String wird
     * @return Hinzugefuegter String mit der vorgegeber Laenge
     */
    private static String pad(String inString, int requiredLength, char insertedChar) {
        StringBuilder builder = new StringBuilder(inString);
        while (builder.length() < requiredLength) {
            builder.append(insertedChar);
        }
        return builder.toString();
    }

    /**
     * Ergibt lesbare Repräsentation des Saisonkalenders
     * @return String, der Saisonkalenders zeigt
     */
    String stringify() {
        int maxLength = this.getLongestProductName() + 2; // +2 because of ": "
        StringBuilder builder = new StringBuilder("");
        builder.append(pad(builder.toString(), maxLength, ' '));
        builder.append("JFMAMJJASOND\n");
        for (Product product : this.products) {
            builder.append(pad(product.getName() + ": ", maxLength, ' '));
            builder.append(product.stringifyIsRegional() + "\n");
            builder.append(pad("", maxLength, ' '));
            builder.append(product.stringifyIsImported());
            builder.append('\n');
        }
        return builder.toString();
    }

}
