-- Aufgabe 3
-- Teilaufgabe a
data MultTree a = Leaf a | Node (a, a) [MultTree a] deriving Show

t1 :: MultTree Int 
t1 = Node (3 ,42) [Node (3, 15) [Leaf 3, Leaf 11, Leaf 12], Node (49 ,42) [Leaf 42, Leaf 23]]


-- Teilaufgabe b
len :: [a] -> Int
len [] = 0
len (x : xs) = 1 + (len xs)

verzweigungsgrad :: MultTree a -> Int
verzweigungsgrad (Leaf val) = 0
verzweigungsgrad (Node (val1, val2) []) = 0
verzweigungsgrad (Node (val1, val2) (mt : mts)) = max (len mts + 1)  (max (verzweigungsgrad mt) (verzweigungsgrad (Node (val1, val2) mts)))

-- Teilaufgabe c
datenListe :: MultTree a -> [a]
datenListe (Leaf val) = [val]
datenListe (Node (val1, val2) []) = []
datenListe (Node (val1, val2) (mt : mts)) = datenListe mt ++ datenListe (Node (val1, val2) mts)

-- TeilAufgabe d
datenIntervalle :: MultTree Int -> MultTree Int 
datenIntervalle (Leaf val) = Leaf val
datenIntervalle (Node (val1, val2) []) = changeValuesInNode (Node (val1, val2) [])
datenIntervalle (Node (val1, val2) mts) = changeValuesInNode (Node (val1, val2) (changeEachElementOf mts))

-- set max, min value to each Node in a list of node
changeEachElementOf :: [MultTree Int] -> [MultTree Int]
changeEachElementOf [] = []
changeEachElementOf (mt : mts) = datenIntervalle mt : (changeEachElementOf mts)

-- set max value of data list, which this node contains, to the max value of this node
-- and min value of data list to min value of this node
changeValuesInNode :: MultTree Int -> MultTree Int 
changeValuesInNode (Leaf val) = Leaf val
changeValuesInNode (Node (val1, val2) mts) | dataListOfNode (Node (val1, val2) mts) == [] && 
                                                (val1 == max val1 val2) = Node (maxBound, minBound) mts
                                           | dataListOfNode (Node (val1, val2) mts) == [] && 
                                                (val1 == min val1 val2) = Node (minBound, maxBound) mts                                               
                                           | val1 == max val1 val2 = Node (maxData, minData) mts
                                           | val1 == min val1 val2 = Node (minData, maxData) mts
    where maxData = maxOf (dataListOfNode (Node (val1, val2) mts))
          minData = minOf (dataListOfNode (Node (val1, val2) mts))

-- Create a list of data (from data node - leaf), which this node contains
dataListOfNode :: MultTree Int -> [Int]
dataListOfNode (Leaf val) = [val]
dataListOfNode (Node (val1, val2) []) = []
dataListOfNode (Node (val1, val2) (mt : mts)) = valueOfLeaf mt ++ dataListOfNode (Node (val1, val2) mts)
    where valueOfLeaf :: MultTree Int -> [Int]
          valueOfLeaf (Leaf val) = [val]
          valueOfLeaf (Node (val1, val2) _) = [] 

maxOf :: [Int] -> Int
maxOf [x] = x
maxOf (x : xs) = max x (maxOf xs)

minOf :: [Int] -> Int 
minOf [x] = x
minOf (x : xs) = min x (minOf xs)


-- Teilaufgabe e
contains :: Int -> MultTree Int -> Bool 
contains intVal mt | (intVal == maxBound ) || (intVal == minBound ) = containsInNode mt intVal
                   | otherwise = isElementOf (datenListe mt) intVal 

containsInNode :: MultTree Int -> Int -> Bool 
containsInNode (Leaf val) intVal = val == intVal
containsInNode (Node (val1, val2) mts) intVal | intVal == val1 || intVal == val2 = True 
                                              | otherwise = checkEachNodeOf mts intVal
-- check every node in list of node if a int value is in there
checkEachNodeOf :: [MultTree Int] -> Int -> Bool 
checkEachNodeOf [] _ = False
checkEachNodeOf (mt : mts) intVal | contains intVal mt = True 
                                  | otherwise = checkEachNodeOf mts intVal

isElementOf :: [Int] -> Int -> Bool 
isElementOf [] _ = False 
isElementOf (x : xs) val | x == val = True 
                         | otherwise = isElementOf xs val