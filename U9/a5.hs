f 1 ys _ = ys
f x (y : ys) z = if x > z then f (x - 1) (x : ys) z else (y : ys)


g x (y : ys) = g (y x) ys
g x y = x []

h [] x y = if x == 1 then h y (length y) [] else True
h (a : as) x y = h as (x + a) y
