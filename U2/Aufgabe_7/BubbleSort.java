public class BubbleSort {
	//sort ascending from low to high
	public static void sort(int[] a) {
		int temp;
		boolean swapped = true;
		for (int i = 0; i < a.length; i++) {
			swapped = true;
			// j is end limit for each pass of sort
			for (int j = 0; j <  a.length - i - 1; j++) {
				//swap 2 elements if they're not in right order
				if (a[j] > a[j + 1]) {
					temp = a[j];
					a[j] = a[j + 1];
					a[j + 1] = temp;

					//if section runs means elements needed to be sorted
					swapped = false;
				}
			}

			// if all the elements are on right order then break.
			if (swapped) {
				break;
			}
		}
	}
}

