import java.util.Arrays;
import java.util.*;

public class Hoare_Aufgabe_9 {
    static boolean isInArray(int element, int[] array, int fromIndex, int toIndex) {
        int[] subArray = Arrays.copyOfRange(array, fromIndex, toIndex);
        for (int i : subArray) {
            if (i == element) {
                return true;
            }
        }
        return false;
    }

    public static void main(String[] args) {
        Random rand = new Random(System.currentTimeMillis());
        int length = rand.nextInt(10);
        int[] a = new int[length]; 
        for (int j = 0; j < length; j++) {
            a[j] = rand.nextInt(20);
        }

        Scanner scanner = new Scanner(System.in);
        System.out.print("x = ");
        int x = scanner.nextInt();
        scanner.close();
        assert true;
        assert true && (0 == 0) && (false == false);
        boolean res = false;
        assert true && (0 == 0) && (res == false);
        int i = 0;
        assert (i == 0) && (res == false);
        assert res == isInArray(x, a, 0, i + 1) && (i <= length);
        while (i < a.length) {
            assert res == isInArray(x, a, 0, i) && (i <= length) && (i < a.length);
            if (x == a[i]) {
                assert res == isInArray(x, a, 0, i) && (i <= length) && (i < a.length) && (x == a[i]);
                assert true == isInArray(x, a, 0, i + 1) && (i + 1 <= length);
                res = true;
                assert res == isInArray(x, a, 0, i + 1) && (i + 1 <= length);
            }
            assert res == isInArray(x, a, 0, i + 1) && (i + 1 <= length);
            i = i + 1;
            assert res == isInArray(x, a, 0, i) && (i <= length);
        }

        assert res == isInArray(x, a, 0, i) && !(i < a.length) && (i <= length);
        assert res == isInArray(x, a, 0, i);
        System.out.println(res);

    }
}
