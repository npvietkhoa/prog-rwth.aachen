import java.util.Scanner;

/**
 * Hoare_Aufgabe_5
 */
public class Hoare_Aufgabe_5 {

    // calculate p^q in integer value
    static int pow(int p, int q) {
        return (q <= 1) ? p : p * pow(p, q - 1);
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int q = scanner.nextInt();
        int p = scanner.nextInt();

        int n, x, y;

        // assert (q >= 1);
        // assert (q >= 1) && (1 == 1) && (p == p) && (q == q);
        n = q;
        // assert (q >= 1) && (1 == 1) && (p == p) && (n == q);
        x = p;
        // assert (q >= 1) && (1 == 1) && (x == p) && (n == q);
        y = 1;
        // assert (q >= 1) && (y == 1) && (x == p) && (n == q);

        // assert (pow(p, q) == pow(x, n) * y) && (n >= 1); 
        while (n > 1) {
            // assert (pow(p, q) == pow(x, n) * y) && (n >= 1) && (n > 1);
            if (n % 2 == 0) {
                // assert (pow(p, q) == pow(x, n) * y) && (n >= 1) && (n > 1) && (n % 2 == 0) ;
                // assert (pow(p, q) == pow(x * x, n / 2) * y) && (n / 2 >= 1);
                x = x * x;
                // assert (pow(p, q) == pow(x, n / 2) * y) && (n / 2 >= 1);
                n = n / 2;
                // assert (pow(p, q) == pow(x, n) * y) && (n >= 1);
            } else {
                // assert (pow(p, q) == pow(x * x, (n - 1) / 2) * (x * y)) && ((n - 1) / 2 >= 1);
                y = x * y;
                // assert (pow(p, q) == pow(x * x, (n - 1) / 2) * y) && ((n - 1) / 2 >= 1);
                x = x * x;
                // assert (pow(p, q) == pow(x, (n - 1) / 2) * y) && ((n - 1) / 2 >= 1);
                n = (n - 1) / 2;
                // assert (pow(p, q) == pow(x, n) * y) && (n >= 1);
            }
        //     assert (pow(p, q) == pow(x, n) * y) && (n >= 1);
        }
        // assert (pow(p, q) == pow(x, n) * y) && !(n > 1) && (n >= 1);
        // assert (pow(p, q) == x * y);
        x = x * y;
        // assert (pow(p, q) == x);

        System.out.println(x);
    }
}