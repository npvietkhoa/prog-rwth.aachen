/**
 * Hoear test case
 */
import java.util.Scanner;
public class Hoare_Aufgabe_3 {

    static int sumOf(int fromValue, int toValue) {
        if (fromValue > toValue) {
            return 0;
        }

        return toValue + sumOf(fromValue, toValue - 1);
    }
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int x = scanner.nextInt();
        scanner.close();
        int res;
        int c;
    
        assert (x >= 0) && (x == x) && (-x == -x);

        res = -x;
        assert (x >= 0) && (x == x) && (res == -x);
        c = x;
        assert (x >= 0) && ( c == x) && (res == -x);

        assert (res == 2*sumOf(c + 1, x) - x) && (c >= 0);

        while(c > 0) {
            assert (res == 2*sumOf(c + 1, x) - x) && (c >= 0) && (c > 0);

            assert (res + 2 * c == 2*sumOf(c, x) - x) && (c - 1 >= 0);
            res = res + 2 * c;
            assert (res == 2*sumOf(c, x) - x) && (c - 1 >= 0);
            c = c - 1;
            assert (res == 2*sumOf(c + 1, x) - x) && (c >= 0);
        }
        assert (res == 2*sumOf(c + 1, x) - x) && !(c > 0) && (c >= 0);
        assert res == x*x;
        System.out.println(res);
    }
}