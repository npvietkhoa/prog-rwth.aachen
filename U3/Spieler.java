public class Spieler {
    Karte[] kartenhand;
    String spielerName;
    double siegesQuote;

    // public Spieler(String name, double quote) {
    //     this.spielerName = name;
    //     this.siegesQuote = quote;
    // }

    public String toString() {
        return this.spielerName;
    }

    //Aufgabe 7h
    static Spieler besterSpieler(Spieler...club) {
        Spieler besteSpieler = new Spieler();
        for (Spieler spieler : club) {
            if (spieler.siegesQuote > besteSpieler.siegesQuote) {
                besteSpieler = spieler;
            }
        }

        return besteSpieler;
    }

    // Aufgabe 7i
    void kannBedienen(Karte k) {
        String outString = this.spielerName + " kann";
        outString += k.bedient(kartenhand) ? " bedienen!" : " nicht bedienen!";
        System.out.println(outString); 
    }

    //Aufgabe 7j
    public static void main(String[] args) {
        Spieler elisa = new Spieler();
        elisa.spielerName = "Elisabeth";
        elisa.siegesQuote = 37.5 / 100;

        Spieler klaus = new Spieler();
        klaus.spielerName = "Klaus";
        klaus.siegesQuote = 12.5 / 100;

        Spieler helmut = new Spieler();
        helmut.spielerName = "Helmut";
        helmut.siegesQuote = 38.75 / 100;

        Spieler erwin = new Spieler();
        erwin.spielerName = "Erwin";
        erwin.siegesQuote = 11.25 / 100;

        Spieler beste = new Spieler();
        beste = besterSpieler(elisa, klaus, helmut, erwin);
        System.out.println(beste.toString());

        Karte[] karteArr = {Karte.neueKarte("HERZ", "SIEBEN"), Karte.neueKarte("HERZ", "NEUN"), Karte.neueKarte("KARO", "KOENIG")};
        beste.kartenhand = karteArr;
        beste.kannBedienen(Karte.neueKarte("KARO", "BUBE"));

        // Karte.druckeEinbahnBedienungen();
    }

}
