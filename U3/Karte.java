public class Karte {
    Farbe farbe;
    Wert wert;

    // Aufgabe 7b
    public String toString() {
        return wert.toString() + farbe.toString();
    }

    // Aufgabe 7c
    public static Karte neueKarte(Farbe f, Wert w) {
        Karte k = new Karte();
        k.farbe = f;
        k.wert = w;
        return k;
    }

    // Aufgabe 7c
    public static Karte neueKarte(String f, String w) {
        return neueKarte(Farbe.valueOf(f), Wert.valueOf(w));
    }

    // Aufgabe 7d
    public static int kombinationen() {
        return Farbe.values().length * Wert.values().length;
    }

    // Aufgabe 7d
    public static Karte[] skatblatt() {
        Farbe farbe[] = Farbe.values();
        Wert wert[] = Wert.values();
        Karte[] skatblatt = new Karte[kombinationen()];
        int index = 0;
        for (Wert w : wert) {
            for (Farbe f : farbe) {
                skatblatt[index] = Karte.neueKarte(f, w);
                index += 1;
            }
        }

        return skatblatt;
    }

    // Aufgabe 7e
    boolean bedient(Karte other) {
        if (this.wert.equals(Wert.BUBE)) {
            return true;
        }

        if (this.wert.equals(other.wert) || this.farbe.equals(other.farbe)) {
            return true;
        }

        return false;
    }

    // Aufgabe 7e
    boolean bedient(Karte... Karte) {
        for (Karte karte2 : Karte) {
            if (karte2.bedient(this)) {
                return true;
            }
        }
        return false;
    }

    //Die Karte sind nicht Bube sind und ihre Farbe sind nicht wie die von Bube => koennen nicht bedienen
    // => sie sind die Karte, die  EinbahnBedienungen conditionen erfuellen
    static void druckeEinbahnBedienungen() {
        for (Farbe farbe : Farbe.values()) {
            Karte bubeKarte = neueKarte(farbe, Wert.BUBE);
            for (Karte karte : Karte.skatblatt()) {                    
                if (karte.wert != Wert.BUBE && karte.farbe != farbe) {
                    System.out.println(bubeKarte.toString() + " bedient " + karte.toString() + 
                                        " , aber " + karte.toString() + " nicht " + bubeKarte.toString());
                }
            }
        }
    }
}
