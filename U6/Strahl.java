/**Aufgabe 8f:
 * Class Strahl
 */
public class Strahl extends Gerade {
    final Punkt anfangsPunkt;

    /**Aufgabe 8f:
     * Konstruktor, um ein Strahl, der mit dem ersten Punkt anfaengt, zu erstellen
     * @param p1 erster Punkt
     * @param p2 zweiter Punkt
     */
    Strahl(Punkt p1, Punkt p2) {
        super(p1, p2);
        this.anfangsPunkt = p1;
    }

    /**Aufgabe 8f:
     * Pruefen ob der Strahl faengt mit p1 Punkt des Gerade an
     * @return true wenn der Strahl mit p1 Punkt des Gerade anfaengt
     */
    boolean startsFromp1() {
        return this.anfangsPunkt.equals(super.p1);
    }

    /**Aufgabe 8f:
     * Pruefen ob der Strahl faengt mit p2 Punkt des Gerade an
     * @return true wenn der Strahl mit p2 Punkt des Gerade anfaengt
     */
    boolean startsFromp2() {
        return this.anfangsPunkt.equals(super.p2);
    }

    /**Aufgabe 8f:
     * Ergibt String, der ueber den Strahl beschreiben kann
     * @return String beschreibt den Strahl
     */
    public String toString() {
        if (this.startsFromp1()) {
            return "Strahl beginnt mit " +
                    this.anfangsPunkt.toString() +
                    " und durch " +
                    super.p2.toString() + ".";
        }

        return "Strahl beginnt mit " +
                this.anfangsPunkt.toString() +
                " und durch " +
                super.p1.toString() + ".";
    }

    /**Aufgabe 8g:
     * Zueruckgib die Gerade, die wenn man den Strahl ueber den Punkt, an dem der Strahl beginnt, ist
     * @return die Gerade, die den Strahl enthaelt
     */
    Gerade verlaengern() {
        return new Gerade(p1, p2);
    }

    /**Aufgabe 8g:
     * Pruf, ob der Strahl den Punkt enthaelt
     * @param p0 der gepruefte Punkt
     * @return true wenn der Strahl den Punkt enthaelt
     */
    @Override
    boolean enthaelt(Punkt p0) {
        Gerade geradeDurchStrahl = new Gerade(p2, p1);
        if (this.startsFromp1()) {
            return !geradeDurchStrahl.vorp1(p0);
        }
        return !geradeDurchStrahl.hinterp2(p0);
    }

    /**Aufgabe 8g:
     * Zeigt die Gleicheit von ein Objekt mit this Strahl
     * @param obj ein Objekt
     * @return true wenn das Objekt und der Strahl gleich sind
     */
    public boolean equals(Object obj) {
        if (obj.getClass().equals(this.getClass())) {
            Strahl objStrahl = (Strahl) obj;

            if (objStrahl.startsFromp2()) {
                return this.enthaelt(objStrahl.p1);
            } else {
                return this.enthaelt(objStrahl.p2);
            }

        }
        return false;
    }
    
}
