/**Aufgabe 8h:
 * Klasse Strecke
 */
public class Strecke extends Strahl {
    /**Aufgabe 8h:
     * Konstruktor, um ein Strecke zu erstellen
     * @param p1 erster Punkt des Strecke
     * @param p2 zweiter Punkt des Streck
     */
    Strecke(Punkt p1, Punkt p2) {
        super(p1, p2);
    }

    /**Aufgabe 8i:
     * Zeigt die Gleicheit von ein Objekt mit this Strecke
     * @param obj ein Objekt
     * @return true wenn das Objekt und diese Strecke gleich sind
     */
    @Override
    public boolean equals(Object obj) {
        if (obj.getClass().equals(this.getClass())) {
            Strecke objStrecke = (Strecke) obj;
            return this.p1.equals(objStrecke.p1) && this.p2.equals(objStrecke.p2);
        }
        return false;
    }

    /**Aufgabe 8h:
     *
     * @param swap
     * @return
     */
    Strahl verlaengern (boolean swap) {
        if (swap) {
            Strahl strahlp2p1 = new Strahl(p2, p1);
            return strahlp2p1;
        }
        return new Strahl(p1, p2);
    }

    @Override
    boolean enthaelt(Punkt p0) {
        Gerade geradeDurchStreke = new Gerade(p1, p2);
        return geradeDurchStreke.zwischenp1p2(p0);
    }

    @Override
    public String toString() {
        return "Strecke beginnt mit " +
                this.p1.toString() +
                " und endet mit " +
                this.p2.toString() + ".";
    }

}
