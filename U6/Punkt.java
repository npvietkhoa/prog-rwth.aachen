import java.math.*;
/** Aufgabe 8a:
 * Punkt
 */
public class Punkt {

    final BigDecimal x;
    final BigDecimal y;

     /**Aufgabe 8a:
     * Konstruktor mit BigDecimal Parameters
     * @param x x-Koordination
     * @param y y-Koordination
     */
    Punkt(BigDecimal x, BigDecimal y) {
        this.x = x;
        this.y = y;
    }

     /**Aufgabe 8a:
     * Konstruktor von Punkt mit double Parameters
     * @param x x-Koordination
     * @param y y-Koordination
     */
    Punkt(double x, double y) {
        this(BigDecimal.valueOf(x), BigDecimal.valueOf(y));
    }

    /** Aufgabe 8a:
     * Ergibt ein String, um die Punkte auszudrueken
     * @return String beschreibt den Punkt
     */
    public String toString() {
        return "(" + this.x + "," + this.y + ")";
    }

    /** Aufgabe 8b:
     * Rechnen den Abstand zwischen this Punkt und other Punkt
     * @param other Andere Punkt, die Abstand dazwischen zu rechnen
     * @return Abstand zwischen 2 Punkte in 2 Dimension
     */
    BigDecimal abstand(Punkt other) {
        BigDecimal xQuadrierteDifferenz  = (this.x.add(other.x.negate())).pow(2);
        BigDecimal yQuadrierteDifferenz = (this.y.add(other.y.negate())).pow(2);
        return BigDecimalUtility.sqrt(xQuadrierteDifferenz.add(yQuadrierteDifferenz));
    }

    /**Aufgabe 8b:
     *Zeigt die Gleicheit von ein Objekt mit this Punkt
     * @param obj ein andere Objekt
     * @return die Gleichheit
     */
    public boolean equals(Object obj) {
        if (obj instanceof Punkt) {
            Punkt objPunkt = (Punkt) obj;
            return this.x.compareTo(objPunkt.x) == 0 && this.y.compareTo(objPunkt.y) == 0;
        }
        return false;
    }
}