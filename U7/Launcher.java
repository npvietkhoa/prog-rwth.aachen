public class Launcher {
    public static void main(String[] args) {
        MutableMap<String, Integer> map = new MutableMap<>();
        putEntries(map);
        printEntries(map);

        ImmutableMap<String, Integer> immutableMap = map.asImmutableMap();
        printEntries(immutableMap);
    }

    static void putEntries(WriteableMap<String, Integer> writeableMap) {
        writeableMap.put("sizeInMB", 42);
        writeableMap.put("version", 4);
        writeableMap.put("yearOfRelease", 2015);
    }

    static void printEntries(ReadableMap<String, Integer> readableMap) {
        try {
            for (String key :
                    readableMap.keyAsSet()) {
                System.out.println(key + ": " + readableMap.getOrThrow(key));
            }
        } catch (UnknownKeyException uke) {
            System.out.println("There is no such key in map or key is null");
        }

    }
}
