public class MutableMap<K, V> extends AbstractReadableMap<K, V> implements WriteableMap<K, V> {


    @Override
    public void put(K key, V value) {
        for (int i = 0; i < this.entries.length; i++) {
            if (entries[i] == null || entries[i].getKey().equals(key)) {
                entries[i] = new Entry<>(key, value);
                return;
            }
        }
        int oldSize = entries.length;
        this.entries = GenericArrayHelper.copyArrayWithIncreasedSize(entries, oldSize * 2);
        this.entries[oldSize] = new Entry<>(key, value);
    }
}