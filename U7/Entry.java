public class Entry<K, V> {
    private final K key;
    private final V value;

    Entry(K key, V value) {
        this.key = key;
        this.value = value;
    }

    V getValue() {
        return this.value;
    }

    K getKey() {
        return this.key;
    }
}
