import java.util.HashSet;
import java.util.Set;

public abstract class AbstractReadableMap<K, V> implements ReadableMap<K, V> {
    protected Entry<K, V>[] entries;

    AbstractReadableMap(Entry<K, V>[] entries) {
        this.entries = GenericArrayHelper.copyArray(entries);
    }

    AbstractReadableMap() {
        this.entries = GenericArrayHelper.newEntryArrayOfSize(10);
    }

    @Override
    public V getOrThrow(K key) throws UnknownKeyException {
        if (key == null) throw new UnknownKeyException();
        for (Entry<K, V> entry : entries) {
            if (key.equals(entry.getKey())) {
                return entry.getValue();
            }
        }
        throw new UnknownKeyException();
    }

    @Override
    public ImmutableMap<K, V> asImmutableMap() {
        return new ImmutableMap<>(this.entries);
    }

    @Override
    public Set<K> keyAsSet() {
        HashSet<K> keySet = new HashSet<>();
        for (Entry<K, V> entry :
                entries) {
            if (entry == null) break;
            keySet.add(entry.getKey());
        }
        return keySet;
    }
}
